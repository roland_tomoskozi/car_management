package at.java.config;

import at.java.aspects.PerformanceAlertAspect;
import at.java.repositories.CarRepository;
import at.java.repositories.NumberPlateRepository;
import at.java.services.CarService;
import at.java.services.CarServiceJpaImpl;
import at.java.services.CarServiceMemoryImpl;
import at.java.services.CarServiceRepoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by schulung on 17.11.16.
 */
@Configuration
public class ServicesConfiguration {
    @Autowired
    private CarRepository carRepo;

    @Autowired
    private NumberPlateRepository numberPlateRepository;

    @Bean(name = "carService")
    @Scope("singleton")
    public CarService produceCarService() {
        CarServiceRepoImpl service = new CarServiceRepoImpl(carRepo, numberPlateRepository);
        return service;
    }

    @Bean
    public PerformanceAlertAspect performanceAlert() {
        return new PerformanceAlertAspect();
    }

}
