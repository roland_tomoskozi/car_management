package at.java.config;

import at.java.entities.Car;
import at.java.entities.NumberPlate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import java.math.BigInteger;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by schulung on 17.11.16.
 */
@Configuration
public class TestDataConfiguration {

    public static final int NO_CARS = 10;

    @Bean
    public Car someCar() {
        return new Car(null, "Porsche", "718", YearMonth.of(2018, 9), NumberPlate.District.I, "123R");
    }

    @Bean()
    public Car singletonCar() {
        Car car = new Car(null, "Porsche", "718", YearMonth.of(2018, 9), NumberPlate.District.I, "123R");
        return car;
    }

    @Bean
    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public NumberPlate sessionNumberPlate() {
        return new NumberPlate("123R", NumberPlate.District.I);
    }

    @Bean(name = "testDataCarList")
    @Scope("prototype")
    public List<Car> produceCarListTestData() {
        List<Car> result = new ArrayList<>();
        for (long i = 0; i < NO_CARS; i++) {
            result.add(new Car(i, "Brand" + i, "Model" + i, YearMonth.now(), NumberPlate.District.I, i + "Y"));
        }
        return result;
    }

    @Bean(name = "testDataCarMap")
    @Scope("prototype")
    public Map<Long, Car> produceCarMapTestData() {
        return produceCarListTestData().stream().collect(Collectors.toMap(c -> c.getId(), c -> c));
    }
}
