package at.java.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by schulung on 19.11.16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = CarValidator.class)
public @interface ValidCar {
    String message() default "Car not Valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
