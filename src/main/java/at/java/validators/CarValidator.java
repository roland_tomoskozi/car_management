package at.java.validators;

import at.java.entities.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by schulung on 19.11.16.
 */
public class CarValidator implements ConstraintValidator<ValidCar, Car> {
    String defaultMessage;

    @Autowired
    MessageSource messageSource;

    @Override
    public void initialize(ValidCar constraintAnnotation) {
        defaultMessage = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Car car, ConstraintValidatorContext context) {
        boolean result;
        if (car.getBrand().toLowerCase().contains("porsche") && car.getHorsepowers() < 300) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate() + ": A Porsche must have more power.").addPropertyNode("horsepowers").addConstraintViolation();
            result = false;
        } else {
            result = true;
        }
        return result;
    }
}
