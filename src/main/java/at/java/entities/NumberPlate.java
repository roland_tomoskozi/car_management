package at.java.entities;

import at.java.controller.rest.NumberPlateJSonSerializer;
import at.java.controller.rest.NumberPlateJsonDeserializer;
import at.java.converter.NumberPlateFormatter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Locale;

/**
 * Created by schulung on 18.11.16.
 */
@Entity
@Table(name = "NUMBER_PLATE")
@JsonDeserialize(using = NumberPlateJsonDeserializer.class)
@JsonSerialize(using = NumberPlateJSonSerializer.class)
@Data
@NoArgsConstructor
public class NumberPlate implements Serializable {
    public enum District {I, IM, IL, KB, KU, LA, LZ, RE, SZ}

    @Pattern(regexp = "\\d+[A-Z]")
    @Id
    @Column(name = "ID")
    private String number;

    @NotNull
    @Column(name = "DISTRICT")
    @Enumerated(EnumType.STRING)
    private District district;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "CAR_ID")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Car car;


    public NumberPlate(String number, District district) {
        this.number = number;
        this.district = district;
    }

    @Override
    public String toString(){
        return new NumberPlateFormatter().print(this, Locale.getDefault());
    }
}
