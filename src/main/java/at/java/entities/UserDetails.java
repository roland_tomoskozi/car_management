package at.java.entities;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by schulung on 22.11.16.
 */
@Data
public class UserDetails {
    private String username;
    private String password;
    private List<GrantedAuthority> authorities;

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = new ArrayList(authorities);
    }
}
