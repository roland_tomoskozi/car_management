package at.java.entities.modifications;

public enum DesignUpgrade {
    SPOILER, RUNNING_BOARD, UNDERCOAT, LEATHHER_SEATS
}
