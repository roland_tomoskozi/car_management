package at.java.entities.modifications;

public enum Tuning {
    CHIPTUNING(50),
    RACE_TURBO_CHARGER(100),
    RACE_EXHAUST(20);

    private int additionalHP;

    private Tuning(int additionalHP){
        this.additionalHP = additionalHP;
    }

    public int getAdditionalHP() {
        return additionalHP;
    }
}
