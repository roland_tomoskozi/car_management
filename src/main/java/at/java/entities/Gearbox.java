package at.java.entities;

/**
 * Created by schulung on 18.11.16.
 */
public enum Gearbox {
    AUTOMATIC, MANUAL
}
