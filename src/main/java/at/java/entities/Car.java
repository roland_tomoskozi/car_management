package at.java.entities;

import at.java.controller.rest.YearMonthJsonDeserializer;
import at.java.controller.rest.YearMonthJsonSerializer;
import at.java.entities.modifications.DesignUpgrade;
import at.java.entities.modifications.Tuning;
import at.java.validators.ValidCar;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.YearMonth;
import java.util.Set;

/**
 * Created by schulung on 16.11.16.
 */
@ValidCar
@Entity
@Table(name = "CAR")
@NamedQueries({
        @NamedQuery(name = "Car.selectAll", query = "select c from Car c"),
        @NamedQuery(name = "Car.selectById", query = "select c from Car c where c.id = :id")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name="CarFetchAll", includeAllAttributes = true)
})
@Data
public class Car implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Size(min = 4, max = 10)
    @Column(name = "BRAND")
    private String brand = "";

    @Size(min = 4, max = 10)
    @Column(name = "MODEL")
    private String model = "";
    @NotNull
    @Column(name = "BUILD_DATE")
    private YearMonth buildDate = YearMonth.now();
    @Valid
    @OneToOne(mappedBy = "car", cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.PERSIST})
    private NumberPlate numberPlate;

    @NotNull
    @Column(name = "GEARBOX")
    @Enumerated(EnumType.STRING)
    private Gearbox gearbox = Gearbox.MANUAL;

    @Min(75)
    @Max(500)
    @Column(name = "HORSEPOWERS")
    private int horsepowers = 100;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "TUNINGS", joinColumns = @JoinColumn(name = "CAR_ID"))
    @Enumerated(EnumType.STRING)
    @Column(name = "TUNING")
    Set<Tuning> tunings;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "DESIGN_UPGRADES", joinColumns = @JoinColumn(name = "CAR_ID"))
    @Enumerated(EnumType.STRING)
    @Column(name = "DESIGN_UPGRADE")
    Set<DesignUpgrade> designUpgrades;

    public Car() {
    }

    public Car(Long id, String brand, String model, YearMonth buildDate, NumberPlate.District district, String number) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.buildDate = buildDate;
        this.numberPlate = new NumberPlate(number, district);
    }


    @JsonSerialize(using = YearMonthJsonSerializer.class)
    public YearMonth getBuildDate() {
        return buildDate;
    }

    @JsonDeserialize(using = YearMonthJsonDeserializer.class)
    public void setBuildDate(YearMonth buildDate) {
        this.buildDate = buildDate;
    }


    public void setNumberPlate(NumberPlate numberPlate) {
        this.numberPlate = numberPlate;
        numberPlate.setCar(this);
    }

    public Gearbox getGearbox() {
        return gearbox;
    }


}
