package at.java.services;

import at.java.entities.Car;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by schulung on 17.11.16.
 */

public class CarServiceMemoryImpl implements CarService {
    AtomicLong nextId = new AtomicLong(1L);

    @Value("#{testDataCarMap}")
    private Map<Long, Car> carMap;

    @Override
    public List<Car> getAllCars() {
        return new ArrayList<>(carMap.values());
    }

    @Override
    public void saveCar(Car car) {
        if (car.getId() == null) {
            car.setId(nextId.getAndIncrement());
        }
        carMap.put(car.getId(), car);
    }

    @Override
    public Car getCarById(Long id) {
        return carMap.get(id);
    }

    @Override
    public void removeCar(Long carId) {
        carMap.remove(carId);
    }
}
