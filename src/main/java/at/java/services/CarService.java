package at.java.services;

import at.java.entities.Car;
import at.java.entities.Gearbox;
import at.java.session.SearchFilter;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by schulung on 16.11.16.
 */
public interface CarService {
    List<Car> getAllCars()  ;

    void saveCar(Car car);

    Car getCarById(Long id);

    default void removeCar(Car car) {
        removeCar(car.getId());
    }

    void removeCar(Long carId);

    default Gearbox[] getGearBoxes(){
        return Gearbox.values();
    }


    @Transactional(readOnly = true)
    default public List<Car> getFilteredCars(SearchFilter filter) {

        String gearboxFilter = filter.getGearboxFilter();
        String modelOrBrandFilter = filter.getModelOrBrandFilter();

        boolean gearBoxFilter = gearboxFilter != null && !"any".equals(gearboxFilter);
        boolean brandOrModelFiler = modelOrBrandFilter != null && !modelOrBrandFilter.trim().isEmpty();

        List<Car> cars = getAllCars();

        if (gearBoxFilter) {
            cars = cars
                    .stream().filter(c -> c.getGearbox().toString().equals(gearboxFilter))
                    .collect(Collectors.toList());
        }

        if (brandOrModelFiler) {
            cars = cars
                    .stream()
                    .filter(c -> c.getBrand().contains(modelOrBrandFilter) || c.getModel().contains(modelOrBrandFilter))
                    .collect(Collectors.toList());
        }

        return cars;
    }

}
