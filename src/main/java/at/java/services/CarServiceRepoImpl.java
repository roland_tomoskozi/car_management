package at.java.services;

import at.java.entities.Car;
import at.java.entities.NumberPlate;
import at.java.repositories.CarRepository;
import at.java.repositories.NumberPlateRepository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by schulung on 24.11.16.
 */
public class CarServiceRepoImpl implements CarService {
    private CarRepository carRepo;
    private NumberPlateRepository numberPlateRepo;

    public CarServiceRepoImpl(CarRepository carRepo, NumberPlateRepository numberPlateRepo) {
        this.carRepo = carRepo;
        this.numberPlateRepo = numberPlateRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Car> getAllCars() {
        List<Car> result = carRepo.findAll();
        return result;
    }

    @Override
    @Transactional
    public void saveCar(Car car) {
        if (car.getId() != null) {
            Car oldCar = carRepo.getOne(car.getId());
            NumberPlate oldNumberPlate = oldCar.getNumberPlate();
            if (!oldNumberPlate.equals(car.getNumberPlate())) {
                numberPlateRepo.deleteById(oldNumberPlate.getNumber());
            }
        }

        car.getNumberPlate().setCar(car);
        carRepo.save(car);
    }

    @Override
    @Transactional(readOnly = true)
    public Car getCarById(Long id) {
        Car result = carRepo.findById(id).orElse(null);
        return result;
    }

    @Override
    @Transactional
    public void removeCar(Long id) {
        carRepo.deleteById(id);
    }
}
