package at.java.services;

import at.java.entities.Car;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by schulung on 20.11.16.
 */
@Repository
public class CarServiceJpaImpl implements CarService {
    @PersistenceContext
    private EntityManager em;


    @Override
    @Transactional(readOnly = true)
    public List<Car> getAllCars() {
        TypedQuery<Car> query = em.createNamedQuery("Car.selectAll", Car.class);
        return query.getResultList();
    }


    @Override
    @Transactional
    public void saveCar(Car car) {
        em.merge(car);
    }

    @Override
    @Transactional(readOnly = true)
    public Car getCarById(Long id) {
        Car car;
        TypedQuery<Car> query = em.createNamedQuery("Car.selectById", Car.class);
        query.setParameter("id", id);
        try {
            car = query.getSingleResult();
        } catch (NoResultException | NonUniqueResultException ex) {
            car = null;
        }
        return car;
    }

    @Override
    @Transactional
    public void removeCar(Long carId) {
        Car car = em.find(Car.class, carId);
        if (car != null) em.remove(car);

    }
}
