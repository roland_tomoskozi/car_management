package at.java.session;

import at.java.entities.Gearbox;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Created by schulung on 21.11.16.
 */
@Component("searchFilter")
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
// same as @SessionSopce, which is available since 4.3
public class SearchFilter {
    private String modelOrBrandFilter;
    private String gearboxFilter;

    public String getModelOrBrandFilter() {
        return modelOrBrandFilter;
    }

    public void setModelOrBrandFilter(String modelOrBrandFilter) {
        this.modelOrBrandFilter = modelOrBrandFilter;
    }

    public String getGearboxFilter() {
        return gearboxFilter;
    }

    public void setGearboxFilter(String gearboxFilter) {
        this.gearboxFilter = gearboxFilter;
    }

    public void take(SearchFilter searchFilter) {
        BeanUtils.copyProperties(searchFilter, this);
    }
}
