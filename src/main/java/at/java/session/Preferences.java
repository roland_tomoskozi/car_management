package at.java.session;

import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Created by schulung on 17.11.16.
 */
@Component("preferences")
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Preferences {
    private boolean showId = false;
    private boolean showBuildDate = true;
    private String highlight = "";

    public boolean isShowId() {
        return showId;
    }

    public void setShowId(boolean showId) {
        this.showId = showId;
    }

    public boolean isShowBuildDate() {
        return showBuildDate;
    }

    public void setShowBuildDate(boolean showBuildDate) {
        this.showBuildDate = showBuildDate;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public void take(Preferences preferncesBean) {
        BeanUtils.copyProperties(preferncesBean, this);
    }

    @Override
    public String toString() {
        return "Preferences{" +
                "showId=" + showId +
                ", showBuildDate=" + showBuildDate +
                ", highlight='" + highlight + '\'' +
                '}';
    }
}
