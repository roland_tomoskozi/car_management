package at.java.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;

import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by schulung on 20.11.16.
 */

@Aspect
public class PerformanceAlertAspect {
    @Value("${performance_threshold}") //see application.properties
    public long threshold = 1L;
    private static final Logger logger = Logger.getLogger(PerformanceAlertAspect.class.getName());

    @Around("execution(* at.java.services.*.*(..))")
    public Object intercept(ProceedingJoinPoint jp) throws Throwable {
        long start = System.currentTimeMillis();
        Object result = jp.proceed();
        long duration = System.currentTimeMillis() - start;

        if (duration >= threshold) {
            String targetClass = jp.getTarget().getClass().getName();
            String targetMethod = jp.getSignature().getName();
            String parameters = Arrays.toString(jp.getArgs());
            String message = String.format("PERFORMANCE ALERT %dms greater than limit %dms, %s.%s arguments: %s", duration, threshold, targetClass, targetMethod, parameters);
            logger.warning(message);
        }

        return result;
    }
}
