package at.java.flows.wizzard;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service("logService")
public class LogService {
    public void logInfo(String message){
        log.info(message);
    }
}
