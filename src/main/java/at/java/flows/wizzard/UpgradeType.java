package at.java.flows.wizzard;

public enum UpgradeType {
    TUNING, DESIGN
}
