package at.java.flows.wizzard;

import at.java.entities.modifications.DesignUpgrade;
import at.java.entities.modifications.Tuning;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UpgradeService {
    public List<Tuning> getAllTunings(){
        return Arrays.asList(Tuning.values());
    }

    public List<DesignUpgrade> getAllDesignUpgrades(){
        return Arrays.asList(DesignUpgrade.values());
    }
}
