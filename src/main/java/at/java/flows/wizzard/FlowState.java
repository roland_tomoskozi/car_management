package at.java.flows.wizzard;

import at.java.entities.Car;
import at.java.entities.modifications.DesignUpgrade;
import at.java.entities.modifications.Tuning;
import at.java.services.CarService;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.security.cert.Extension;

@Data
@NoArgsConstructor
public class FlowState implements Serializable {
    @Autowired
    private transient CarService carService;

    private Car car;
    private UpgradeType upgradeType;
    private Tuning selectedTuning;
    private DesignUpgrade selectedDesignUpgrade;

    public boolean save(){
        if (upgradeType==UpgradeType.TUNING){
            car.getTunings().add(selectedTuning);
        } else {
            car.getDesignUpgrades().add(selectedDesignUpgrade);
        }
        carService.saveCar(car);
        return true;
    }
}
