package at.java.repositories;

import at.java.entities.Car;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * Created by schulung on 21.11.16.
 */
public interface CarRepository extends JpaRepository<Car, Long> {
    @Override
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, value = "CarFetchAll")
    List<Car> findAll();

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, value = "CarFetchAll")
    List<Car> getCarsByModel(String brandOrModel);

    @Query("select c from Car c where c.model like ?1 or c.brand like ?1")
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, value = "CarFetchAll")
    List<Car> getCarsFilteredByModelOrBrand(String brandOrModel);

}
