package at.java.repositories;

import at.java.entities.NumberPlate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by schulung on 24.11.16.
 */
public interface NumberPlateRepository extends JpaRepository<NumberPlate, String> {
}
