package at.java.converter;

import at.java.entities.NumberPlate;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by schulung on 18.11.16.
 */
public class NumberPlateConverter implements Converter<String, NumberPlate> {

    @Override
    public NumberPlate convert(String s) {
        String[] parts = s.split("-");
        NumberPlate.District district = NumberPlate.District.valueOf(parts[0]);
        NumberPlate result = new NumberPlate(parts[1], district);
        return result;
    }
}
