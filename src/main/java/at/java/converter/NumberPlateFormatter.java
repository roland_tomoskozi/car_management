package at.java.converter;

import at.java.entities.NumberPlate;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by schulung on 18.11.16.
 */
public class NumberPlateFormatter implements Formatter<NumberPlate> {
    @Override
    public NumberPlate parse(String s, Locale locale) throws ParseException {
        String[] parts = s.split("-");
        NumberPlate.District district = NumberPlate.District.valueOf(parts[0]);
        NumberPlate result = new NumberPlate(parts[1], district);
        return result;
    }

    @Override
    public String print(NumberPlate numberPlate, Locale locale) {
        return numberPlate.getDistrict() + "-" + numberPlate.getNumber();
    }
}
