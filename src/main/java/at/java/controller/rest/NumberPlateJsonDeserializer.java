package at.java.controller.rest;

import at.java.converter.NumberPlateFormatter;
import at.java.entities.NumberPlate;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.catalina.User;

import java.io.IOException;
import java.text.ParseException;
import java.time.YearMonth;
import java.util.Locale;

/**
 * Created by schulung on 23.11.16.
 */
public class NumberPlateJsonDeserializer extends JsonDeserializer<NumberPlate> {
    @Override
    public NumberPlate deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        String string = node.get(NumberPlateJSonSerializer.STRING_VALUE).asText();
        NumberPlateFormatter formatter = new NumberPlateFormatter();
        NumberPlate result;
        try {
            result = formatter.parse(string, Locale.getDefault());
        } catch (ParseException pe) {
            throw new IOException("Error parsing NumberPlate Json", pe);
        }
        return result;
    }
}
