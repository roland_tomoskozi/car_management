package at.java.controller.rest;

import at.java.entities.Car;
import at.java.services.CarService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by schulung on 23.11.16.
 */
@RestController
@RequestMapping("/rest")
public class CarRestController {
    @Value("#{carService}")
    CarService carService;

    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    public List<Car> allCars() {
        return carService.getAllCars();
    }

    @RequestMapping(value = "/car/{id}", method = RequestMethod.GET)
    public Car carById(@PathVariable("id") Long id) {
        return carService.getCarById(id);
    }

    @RequestMapping(value = "/car", method = {RequestMethod.GET, RequestMethod.POST})
    public void saveCar(@RequestBody Car car) {
        carService.saveCar(car);
    }

    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    public void deleteCar(@PathVariable Long id) {
        carService.removeCar(id);
    }

}
