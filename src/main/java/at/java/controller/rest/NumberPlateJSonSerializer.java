package at.java.controller.rest;

import at.java.converter.NumberPlateFormatter;
import at.java.entities.NumberPlate;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by schulung on 23.11.16.
 */
public class NumberPlateJSonSerializer extends JsonSerializer<NumberPlate> {

    public static final String STRING_VALUE = "stringValue";

    @Override
    public void serialize(NumberPlate value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartObject();
        String stringValue = new NumberPlateFormatter().print(value, Locale.getDefault());
        gen.writeStringField(STRING_VALUE, stringValue);
        gen.writeEndObject();

    }
}
