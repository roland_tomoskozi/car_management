package at.java.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by schulung on 22.11.16.
 */
@Controller
public class LoginController {
    @RequestMapping("/login_form")
    public String loginForm() {
        return "/login_form";
    }
}
