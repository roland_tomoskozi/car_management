package at.java.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by schulung on 16.11.16.
 */
@Controller
public class IndexController {

    @RequestMapping({"/"})
    public String index(Model model) {
        String view_name = "welcome";
        model.addAttribute("view_name", view_name);
        return view_name;
    }
}
