package at.java.controller;

import at.java.entities.Car;
import at.java.entities.Gearbox;
import at.java.entities.NumberPlate;
import at.java.services.CarService;
import at.java.session.Preferences;
import at.java.session.SearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.math.BigInteger;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by schulung on 16.11.16.
 */
@Controller
public class CarController {
    public static final String VIEW_NAME = "view_name";
    Logger logger = Logger.getLogger(CarController.class.getName());


    @Value("#{preferences}")
    private Preferences preferences;

    @Value("#{searchFilter}")
    private SearchFilter searchFilter;

    @Resource(name = "carService")
    private CarService carService;


    @RequestMapping("/car_list")
    public String listCars(Model model) {
        model.addAttribute("cars", carService.getFilteredCars(searchFilter));
        model.addAttribute("preferences", preferences);
        model.addAttribute("searchFilter", searchFilter);
        model.addAttribute("gearboxes", carService.getGearBoxes());
        String viewName = "car_list";
        model.addAttribute(VIEW_NAME, viewName);
        return viewName;
    }

    @RequestMapping("/add_car_form")
    public String addCarForm(Model model) {
        Car newCar = new Car();
        newCar.setBuildDate(YearMonth.now());
        newCar.setNumberPlate(new NumberPlate("123P", NumberPlate.District.IL));
        model.addAttribute("carCommand", newCar);
        model.addAttribute("gearboxes", Gearbox.values());
        String viewName = "add_car";
        model.addAttribute(VIEW_NAME, viewName);
        return viewName;
    }

    @RequestMapping(value = "/car_filter", method = RequestMethod.POST)
    public String filterCars(SearchFilter searchFilter, Model model) {
        this.searchFilter.take(searchFilter);
        return listCars(model);
    }

    @RequestMapping("/edit_car_form/{carId}")
    public String editCarForm(@PathVariable Long carId, Model model) {
        Car car = carService.getCarById(carId);
        model.addAttribute("carCommand", car);
        model.addAttribute("gearboxes", Gearbox.values());
        String viewName="edit_car";
        model.addAttribute(VIEW_NAME, viewName);
        return "/add_car";
    }

    @RequestMapping(value = "/save_car", method = RequestMethod.POST)
    public String addCarDo(Model model, @ModelAttribute(name = "carCommand") @Valid Car carCommand, BindingResult bindingResult) { // Reihenfolge der Parameter ist wichtig. BindingResult muss nach model kommen.
        logger.info("entering addCarDo");
        String result;
        if (bindingResult.hasErrors()) {
            logger.info(bindingResult.getAllErrors().toString());
            //ModelAttribute and BindingResult will be automatically put on request, if name of ModelAttribute is accordingly configured.
            //Otherwise ModelAttribute name must be same as Classname, then no ModelAttribute Annotation is necessary
            model.addAttribute("gearboxes", Gearbox.values());
            model.addAttribute(VIEW_NAME, "edit_car");
            result = "add_car";
            result = "add_car";
        } else {
            logger.info("Adding car: " + carCommand);
            carService.saveCar(carCommand);
            result = "redirect:" + listCars(model);
        }

        return result;
    }

    @RequestMapping("/remove_car/{carId}")
    public String removeCar(@PathVariable Long carId, Model model) {
        carService.removeCar(carId);
        return "redirect:" + listCars(model);
    }

}
