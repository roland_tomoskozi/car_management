package at.java.controller;

import at.java.entities.UserDetails;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by schulung on 22.11.16.
 */
@Controller
public class UserDetailsController {
    @RequestMapping("user_details")
    public String userDetails(Model model) {
        SecurityContext context = SecurityContextHolder.getContext();
        User user = (User) context.getAuthentication().getPrincipal();

        UserDetails details = new UserDetails();
        details.setUsername(user.getUsername());
        details.setAuthorities(user.getAuthorities());
        details.setPassword(user.getPassword());

        model.addAttribute("userDetails", details);

        String viewName = "user_details";
        model.addAttribute("view_name", viewName);
        return viewName;
    }
}
