package at.java.controller;

import at.java.services.CarService;
import at.java.session.Preferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by schulung on 16.11.16.
 */

@Controller
public class PreferencesController {
    Logger logger = Logger.getLogger(PreferencesController.class.getName());

    @Autowired
    private CarService carService;

    @Autowired
    private Preferences preferences;

    @Autowired
    private CarController carController;

    @ModelAttribute("preferences")
    public Preferences getPreferences() {
        return preferences;
    }

    @RequestMapping("/preferences_form")
    public String preferencesForm(Model model) {
        String preferences = "preferences";
        model.addAttribute("view_name", preferences);
        return preferences;
    }

    @RequestMapping(value = "/save_preferences", method = RequestMethod.POST)
    public String savePreferences(Preferences preferencesBean, Model model) {
        preferences.take(preferencesBean);
        logger.info("Save Preferences: " + preferences);
        return "redirect:" + carController.listCars(model);
    }
}
