package at.java;

import at.java.entities.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
    @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
    @AutoConfigureMockMvc
    public class DemoApplicationTests {
        @Autowired
        private MockMvc mvc;

        @Autowired
        @Qualifier("someCar")
        private Car car;

    @Resource(name = "someCar")
    private Car anotherCar;

    @Test
    public void contextLoads() {

        System.out.println(car);
        System.out.println(anotherCar);
    }

    @Test
    public void testRestController() throws Exception {
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("http://localhost:8080/rest/cars");
        final MockHttpServletResponse response = mvc.perform(builder).andReturn().getResponse();
        System.out.println(response.getContentAsString());

    }

}
